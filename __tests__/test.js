/*
Unit test scripts.
*/
// __tests__/test.js
global.React = require('react/addons');

jest.dontMock('../public/scripts/render.js');


describe('MoreButton', function() {
  it('add more article after click', function() {
    var React = require('react/addons');
    var MoreButton = require('../public/scripts/render.js');    
    var TestUtils = React.addons.TestUtils;

    describe('events', function() {
        var   onClickStub;

        beforeEach(function() {           
          onClickStub = jest.genMockFn();
        });

        it('should call onClickStub when the button is clicked', function() {
              var button = TestUtils.renderIntoDocument(
                <MoreButton hasMore={true} onClick={onClickStub}/>
              );
              var buttonTag = TestUtils.findRenderedDOMComponentWithTag(button, 'button');

              TestUtils.Simulate.click(buttonTag);
              expect(onClickStub).toBeCalled();
        });
        
  });
 });  
});