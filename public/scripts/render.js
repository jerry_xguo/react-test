

/**
 * This file provided by Facebook is for non-commercial testing and evaluation purposes only.
 * Facebook reserves all rights not expressly granted.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * FACEBOOK BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
 

var Article = React.createClass({
  render: function() {
    return (
      <div className="article">     
        <p>{this.props.title}<a href={this.props.image } target="_blank"><img src={this.props.image } alt="image" className="thumb"/></a>  </p>      
      </div>
    );
  }
});

var ArticleBox = React.createClass({
  
  loadArticlesFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        var display = [];
        
        //console.log('this.state.index='+this.state.index);
        for(var i =0; i< this.state.index && i<data.length; i++){
            display.push(data[i]);
        }
        this.state.data = data;
        this.setState({displayData: display});
        if(data.length>this.state.index){
          
            this.setState({hasMore: true});
        }
        
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  onClickMore: function() {
  
    if(this.state.index<this.state.data.length){
        this.state.index = this.state.index + 1;      
        var display = this.state.displayData;
        display.push(this.state.data[this.state.index-1]);
        this.setState({displayData: display});
    }
    if(this.state.index==this.state.data.length){
       
       this.setState({hasMore: false});
    }
    
  },
  getInitialState: function() {
   
    return {data: [], index: 1, displayData:[], hasMore:false};
  },
  componentDidMount: function() {

    this.loadArticlesFromServer();    
  },
  render: function() {
    
    return (
      <div className="articleBox">
        <h1>Articles</h1>
        <ArticleList data={this.state.displayData} />
        <MoreButton handleMore ={this.onClickMore} hasMore={this.state.hasMore}/>
      </div>
    );
  }
});

var ArticleList = React.createClass({
  render: function() {
    var ArticleNodes = this.props.data.map(function(article, index) {
      return (        
        <Article title={article.title} image={article.image} key={article.id} />         
      );
    });
    return (
      <div className="articleList">
        {ArticleNodes}
      </div>
    );
  }
});

var MoreButton = React.createClass({
  handleClickMore: function(e) {
    e.preventDefault();
    //console.log("clicked----------------------"+ this.props.handleMore);
    //console.log("onClick----------------------"+ this.props.onClick);
    if(this.props.onClick!=undefined)
        this.props.onClick(this);
    if(this.props.handleMore!=undefined)
        this.props.handleMore();
   
  },  
  render: function() {
    //console.log("hasMore ="+this.props.hasMore);
    
    if(this.props.hasMore){
        return (            
            <button id='more' onClick  ={this.handleClickMore} >More Article</button>      
        );
    }
    else{
        return( 
          <button >The End</button>
        );
    }
    
  }
});


if (typeof require === 'undefined'){
    React.render(
      <ArticleBox url="articles.json"/>,
      document.getElementById('content')
    );
}
else{
    module.exports = MoreButton; 
    
}





