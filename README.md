[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

# Bauer Media Test based on React Tutorial

This is the Bauer Media Test based on React comment box example from [the React tutorial]


### To prepare
There are one simple node.js server implementations included. They all serve static files from `public/` and handle requests to `articles.json` to fetch or add data. 

npm install nodejs -g

## To use
npm start

And visit <http://localhost:3000/>. Try opening multiple tabs! 

## To test
npm test






